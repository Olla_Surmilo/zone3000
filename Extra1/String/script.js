// Есть текст с переносами строк (\n)
// Посчитать количество слов в тексте.
// Подсказка: Задача решается очень просто если сплитать регулярным выражением с ключем /g
// Если регулярных выражений пока не знаешь, делай как знаешь.


var str = 'The\nworld\nis\na\nbook,\nand\nthose\nwho\ndo\nnot\ntravel,\nread\nonly\na\npage.';
var arr = str.split('\n');
console.log(arr.length);


